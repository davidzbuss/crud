const Employee = require('../models/employee');
const EmployeeCtrl = {};
EmployeeCtrl.getEmployees = async (req, res) => {
  const employees = await Employee.find()
  res.json(employees);
}
EmployeeCtrl.createEmploye = async (req, res) => {
  const employee = new Employee(req.body);
  await employee.save();
  res.json({
    status: "Empleado guardado"
  });
}
EmployeeCtrl.getEmployee = async (req, res) => {
  const employee = await Employee.findById(req.params.id);
  res.json(employee);
}
EmployeeCtrl.editEmployee = async (req, res) => {
  const { id } = req.params;
  const employee = {
    name: req.body.name,
    position: req.body.position,
    office: req.body.office,
    salary: req.body.salary
  };
  Employee.findByIdAndUpdate(id,{$set:employee},{new:true});
  res.json({
    status:"Edited Employee"
  })
}
EmployeeCtrl.deleteEmployee = function () {

}
module.exports = EmployeeCtrl;