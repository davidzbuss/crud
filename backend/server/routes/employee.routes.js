const express=require('express');
const router=express.Router();
const employe=require('../controllers/employee.controller');

router.get('/',employe.getEmployees);
router.post('/',employe.createEmploye);
router.get('/:id',employe.getEmployee);
router.put('/:id',employe.editEmployee);
router.delete('/:id',employe.deleteEmployee);
module.exports=router;