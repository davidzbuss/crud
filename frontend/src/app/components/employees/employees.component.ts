import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/services/employee.service';
import { NgForm } from '@angular/forms';
import { Employee } from 'src/app/models/employee';
declare var M:any;

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css'],
  providers: [EmployeeService]
})
export class EmployeesComponent implements OnInit {
  employees:any;
  constructor(private EmployeeService: EmployeeService) { }

  ngOnInit() {
   this.employees= this.getEmployees();
  }
  resetForm(form?: NgForm) {
    if (form) {
      form.reset();
      this.EmployeeService.selectedEmployee = new Employee();

    }
  }
  addEmployee(form:NgForm){
    
    this.EmployeeService.postEmployee(form.value).subscribe(res=>{
      this.resetForm(form);
      M.toast({html:'Save Successfully'});
      this.getEmployees();
    })
  }
  getEmployees(){
    this.EmployeeService.getEmployees()
    .subscribe(res=>{
      this.EmployeeService.employees=res as Employee[];
      console.log(res);
      
    });
  }

}
